﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace SignalRWebApp.Hubs
{
    public class ChatroomHub : Hub
    {
        //public override Task OnConnectedAsync()
        //{
        //    Groups.AddToGroupAsync(Context.ConnectionId, "Test");

        //    return base.OnConnectedAsync();
        //}

        public async Task StartMessage(string user)
        {
            await Clients.All.SendAsync("JoinedRoom", user);
        }
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }
    }
}
